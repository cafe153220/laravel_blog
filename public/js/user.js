$(document).ready(function(){

  $('input').focus(function(){
    $(this).parent().find(".label-txt").addClass('label-active');
  });

  $("input").focusout(function(){
    if ($(this).val() == '') {
      $(this).parent().find(".label-txt").removeClass('label-active');
    };
  });

});
function signin()
{
  var dat = $("#signin").serializeArray().reduce(function(obj, item) {
    obj[item.name] = item.value;
    return obj;}, {});
 
  $.ajax({
    url : Uri+'/user/auth/sign-in',
    type : "POST",
    dataType : "text",
    headers: {

      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      
      },
    data : {
      dat:dat
    },	
    success : function(data){
      var result =JSON.parse(data);
      console.log('signup_return',result['success']);
      if(result.success==true)
      window.location = Uri+'/blog/home';
      else
      {
        $( "div.validation" ).html(list_validation(result.errors));
      }
      
    },
    error : function(){
      alert("Error");
      }
    });  
}
function signup()
{
  var dat = $("#signup").serializeArray().reduce(function(obj, item) {
    obj[item.name] = item.value;
    return obj;}, {});
 
  $.ajax({
    url : Uri+'/user/auth/sign-up',
    type : "POST",
    dataType : "text",
    headers: {

      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      
      },
    data : {
      dat:dat
    },	
    success : function(data){
      var result =JSON.parse(data);
      console.log('signup_return',result['success']);
      if(result.success==true)
      alert('註冊成功');
      else
      {
        $( "div.validation" ).html(list_validation(result.errors));
      }
      
    },
    error : function(){
      alert("Error");
      }
    });  
}
function list_validation(validation)
{
  console.log('validation',validation);
    var html='';
    $.each( validation, function( key, value ) {
      html+=`<div class="alert alert-danger">`;
      html+=value;
      html+='</div>';
    });
    
    
    return html;
}