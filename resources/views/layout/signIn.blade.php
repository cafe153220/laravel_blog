@extends('layout.master')
@section('title',$title)
@section('content')
<script type="text/javascript">
	 	var Uri = "{{ url('/')}}";
</script>
<body class="bg-success">
<div class="container py-5">
    <div class="row">
        <div class="col-md-12">
            
            <div class="row">
                <div class="col-md-6 mx-auto">

                    <!-- form card login -->
                    <div class="card rounded-0">
                        <div class="card-header">
                            <h3 class="mb-0">登入頁面</h3>
                        </div>
                        <div class="card-body">
                            <form class="form" id="signin" role="form" action="{{ url('/user/auth/sign-in') }}" autocomplete="off" novalidate="" method="POST">
                           
                            <div class="validation">
                                
                            </div>
                                <div class="form-group">
                                    <label for="uname1">Email</label>
                                    <input type="text" class="form-control form-control-lg rounded-0" name="email" id="uname1" required="">
                                    <!-- <div class="invalid-feedback">Oops, you missed this one.</div> -->
                                </div>
                                <div class="form-group">
                                    <label>密碼</label>
                                    <input type="password" class="form-control form-control-lg rounded-0" name="password" id="pwd1" required="" autocomplete="new-password">
                                    <!-- <div class="invalid-feedback">Enter your password too!</div> -->
                                </div>
                                <button type="button" style="margin-left:10px;"  class="btn btn-success btn-lg float-right" onClick="signin()" id="btnLogin">登入</button>
                                <a  class="btn btn-info btn-lg float-right" href="{{ url('/user/auth/sign-up') }}" >註冊</a>
                            </form>
                        </div>
                        <!--/card-block-->
                    </div>
                    <!-- /form card login -->

                </div>


            </div>
            <!--/row-->

        </div>
        <!--/col-->
    </div>
    <!--/row-->
</div>
<!--/container-->
</body>

    <!-- <h1>{{$title}}</h1>
    <form action="{{ url('/user/auth/sign-up') }}" method="post">
    Email : <input type="text" name="email" placeholder="Email">
    密碼  : <input type="password" name="password" placeholder="密碼">
    暱稱 : <input type="text" name="nickname" placeholder="暱稱">
    <input type="hidden" name="_token" value=" {{ csrf_token() }} ">
    {!! csrf_field()!!}
    <button type="submit">註冊</button>
    </form> -->
@endsection