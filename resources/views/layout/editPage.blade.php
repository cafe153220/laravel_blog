<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Blog Home - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="{{url("/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" id="bootstrap-css">

    <!-- Custom styles for this template -->
    <link href="{{url("css/blog-home.css")}}" rel="stylesheet">

  </head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">{{ $user_name }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="{{ url('/blog/post')}}">新增文章
              </a>
            </li>
           
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/user/auth/sign-out') }}">登出</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <h1 class="my-4">
          </h1>
<div class="container">
<div class="card">
          <div class="card-header">訂單管理</div>
          <div class="card-body">
          <form id="edit-profile" class="form-horizontal"  enctype="multipart/form-data" action="" method="post" >
									<fieldset>

										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="order_name">交貨地點</label>
											<input type="text" class="form-control" required="required" id="order_location" name="order_location" >
											</div>
											<div class="form-group col-md-6">
											<label for="order_num">交貨日期</label>
											<input type="date" class="form-control" id="order_date" name="order_date" >
											</div>
										</div>
										
										<div class="form-row">
											<div class="form-group col-md-6">
											<label for="cost">進貨成本</label>
											<input type="text" class="form-control" id="cost" name="order_cost">
											</div>
											<div class="form-group col-md-6">
											<label for="wealth">訂單收益</label>
											<input type="text" class="form-control" id="wealth" name="order_wealth" readonly="readonly">
											</div>
										</div>
                                        <div class="control-group">											
											<label class="control-label" for="lastname">訂單備註</label>
											<div class="controls">
												<textarea name="order_remark" id="order_remark"  rows="10" cols="80" class="form-control"></textarea>
                                            </div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                        
										
                                </div><!--widge-content-->
											
										
									</fieldset>
								
          </div> 
          <div class="card-footer">
		  <button type="submit" class="btn btn-primary">Save</button> 
		  <a class="btn">Cancel</a>
		  </div>
		  </form>
</div>
</div>
</body>
                
		
    
