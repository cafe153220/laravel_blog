<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="_token" content="{{ csrf_token() }}"/>
        <title>@yield('title') -Blog Laravel</title>
        <link href="{{url("/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" id="bootstrap-css">
    
    <script src="{{url("/bootstrap/js/bootstrap.bundle.js")}}"></script>
    <script src="{{url("/bootstrap/js/bootstrap.min.js")}}"></script>
    <script src="{{url("/jquery/jquery.slim.min.js")}}"></script>
    <script src="{{url("/jquery/jquery.js")}}"></script>
    <script src="{{url("/js/user.js")}}"></script>
    </head>
    
            @yield('content')
      
</html>
