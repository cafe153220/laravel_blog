<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchandiseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchandise', function (Blueprint $table) {
            //商品編號
            $table->increments('merchandise_id');
            //商品狀態
            $table->string('status',1)->default('C');
            //-C: 建立中
            //-S: 可販售
            
            //商品名稱
            $table->string('name')->nullable();
            //商品介紹
            $table->text('introduction');
            //商品照片
            $table->string('photo',50)->nullable();
            //價格
            $table->integer('price')->default(0);
            //商品剩餘數量
            $table->integer('remain_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchandise');
    }
}
