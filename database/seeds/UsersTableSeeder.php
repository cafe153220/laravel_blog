<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name=str_random(3);
        DB::table('users')->insert([
            'nickname' => 'account_'.$name,
            'email' => 'account_'.$name.'@gmail.com',
            'password' => Hash::make('acc'.$name),
            'type' =>'G'
        ]);
    }
}
