<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//首頁
Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', function () {
    return view('test');
});
//使用者
Route::group(['prefix'=>'user'],function(){
    Route::group(['prefix'=>'auth'],function(){
        Route::get('/sign-up','UserAuthController@signUpPage');
        Route::post('/sign-up','UserAuthController@signUpProcess');
        Route::get('/sign-in','UserAuthController@signInPage');
        Route::post('/sign-in','UserAuthController@signInProcess');
        Route::get('/sign-out','UserAuthController@signOut');
    });
});
Route::group(['middleware' => ['user.auth.login']], function () {
    Route::group(['prefix'=>'blog'],function(){
        Route::get('/home','BlogController@home');
        Route::get('/post','BlogController@postPage');
        Route::post('/post','BlogController@postProcess');
    });
});
