<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function home()
    {
        $bind=['user_name'=> session()->get('user_name')];
        return view('layout.blog_master',$bind);
    }
    public function postPage()
    {
        $bind=['user_name'=> session()->get('user_name')];
        return view('layout.editPage',$bind);
    }
}
