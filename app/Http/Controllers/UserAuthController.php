<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Hash;
use App\blog\orm\User;

class UserAuthController extends Controller
{
    public function signUpPage()
    {
        $binding=[
            'title' => '註冊'
        ];
        return view('layout.signUp',$binding);
    }
    public function signUpProcess()
    {
        $input=request()->input('dat');
        
        $rules=[
            'nickname'=>[
                'required',
                'max:50',
            ],
            'email' =>[
                'required',
                'max:150',
                'email',
            ],
            'password' =>[
                'required',
                'same:password_confirmation',
                'min:6',
            ],
            'password_confirmation' =>[
                'required',
                'min:6'
            ],
            'type' =>[
                'required',
                'in:G,A'
            ],
        ];
        $validator = Validator::make($input,$rules);

        if($validator->fails()){
            $data=['success'=>false,'errors'=>$validator->errors()->all()];
            return response()->json($data);
        }
        $input['password']=Hash::make($input['password']);
        $User=User::create($input);
        return response()->json(['success'=>true]);
    }
    public function signInPage()
    {
        $binding=[
            'title' => '登入'
        ];
        return view('layout.signIn',$binding);
    }
    public function signInProcess()
    {
        $input=request()->input('dat');
        
        $rules=[
            'email' =>[
                'required',
                'max:150',
                'email',
            ],
            'password' =>[
                'required',
                'min:6'
            ],
            
        ];
        $validator = Validator::make($input,$rules);

        if($validator->fails()){
            $data=['success'=>false,'errors'=>$validator->errors()->all()];
            return response()->json($data);
        }
        $user = User::where('email', $input['email'])->first();
        
        if($user!=null && Hash::check($input['password'],$user->password)) 
        {
            session()->put('user_id',$user->user_id);
            session()->put('user_name',$user->nickname);
            return response()->json(['success'=>true]);
        }
        $errors['0']='帳號或密碼錯誤';
        $data=['success'=>false,'errors'=>$errors];
        return response()->json($data);

    }

    public function signOut()
    {
        session()->flush();
        return redirect(url('user/auth/sign-in'));
    }
}
