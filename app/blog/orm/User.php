<?php

namespace App\blog\orm;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table='users';

    protected $primarykey='id';

    protected $fillable=[
        "email",
        "password",
        "type",
        "nickname",
    ];
}
